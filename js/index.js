document.getElementById("btn-1").addEventListener("click", function (e) {
  document.getElementById("card-1").classList.add("visually-hidden");
  document.getElementById("card-2").classList.remove("visually-hidden");
  document.getElementById("side-pane").classList.remove("visually-hidden");
});

document.getElementById("btn-back-2").addEventListener("click", function (e) {
  document.getElementById("card-1").classList.remove("visually-hidden");
  document.getElementById("card-2").classList.add("visually-hidden");
  document.getElementById("side-pane").classList.add("visually-hidden");
});

const crossouts = document.querySelectorAll(".cross-out");
for (var i = 0; i < crossouts.length; i++) {
  crossouts[i].addEventListener("click", function (e) {
    console.log(e.target);
    if (e.target.style.getPropertyValue("text-decoration") == "line-through") {
      e.target.style.setProperty("text-decoration", "none");
    } else {
      e.target.style.setProperty("text-decoration", "line-through");
    }
  });
}

const nextBtns = document.querySelectorAll(".btn-next");
for (var i = 0; i < nextBtns.length; i++) {
  nextBtns[i].addEventListener("click", function (e) {
    document
      .getElementById("card-" + e.target.id.slice(-1))
      .classList.add("visually-hidden");
    document
      .getElementById("card-" + (Number(e.target.id.slice(-1)) + 1))
      .classList.remove("visually-hidden");
  });
}

const backBtns = document.querySelectorAll(".btn-back");
for (var i = 0; i < backBtns.length; i++) {
  backBtns[i].addEventListener("click", function (e) {
    document
      .getElementById("card-" + e.target.id.slice(-1))
      .classList.add("visually-hidden");
    document
      .getElementById("card-" + (Number(e.target.id.slice(-1)) - 1))
      .classList.remove("visually-hidden");
  });
}

document.getElementById("btn-verify").addEventListener("click", function (e) {
  if (document.getElementById("name-input").value.toLowerCase() == "supay") {
    alert("You Found the Culprit! Supay stole the cookie jar.");
  } else {
    alert("Try Again, the Culprit is still on the loose!");
  }
});
